"""
Project: OsuData

License MIT
Copyright © 2020-2021 - LostPy
"""

import numpy as np
import pandas as pd

import peace_performance_python.prelude as osu_prelude

# Class:
class Beatmap(osu_prelude.Beatmap):
	"""Class to represent a beatmap with this data."""
	NO_MOD_MAX_CALCULATOR = osu_prelude.Calculator(acc=100)

	def __init__(self, path: str, **kwargs):
		"""Use static method `from_beatmap` to create a Beatmap objects with a .osu file."""
		super().__init__(path)
		self._default_calculate()

	def __repr__(self) -> str:
		"""Return the representation of the dataframe of metadata."""
		return self.to_dataframe().__repr__()

	def __str__(self) -> str:
		"""Return the str of the dataframe of metadata."""
		return self.__repr__()

	def __len__(self) -> float:
		"""Return the time of the beatmap."""
		return self.time

	def __eq__(self, obj) -> bool:
		"""Compare the difficulty."""
		if isinstance(obj, Beatmap):
			return self.stars == obj.stars

		raise TypeError("You can't compare an instance of Beatmap with another object")

	def __ne__(self, obj) -> bool:
		"""Compare the difficulty."""
		if isinstance(obj, Beatmap):
			return self.stars != obj.stars

		raise TypeError("You can't compare an instance of Beatmap with another object")

	def __gt__(self, obj) -> bool:
		"""Compare the difficulty."""
		if isinstance(obj, Beatmap):
			return self.stars > obj.stars

		raise TypeError("You can't compare an instance of Beatmap with another object")

	def __ge__(self, obj) -> bool:
		"""Compare the difficulty."""
		if isinstance(obj, Beatmap):
			return self.stars >= obj.stars

		raise TypeError("You can't compare an instance of Beatmap with another object")

	def __lt__(self, obj) -> bool:
		"""Compare the difficulty."""
		if isinstance(obj, Beatmap):
			return self.stars < obj.stars

		raise TypeError("You can't compare an instance of Beatmap with another object")

	def __le__(self, obj) -> bool:
		"""Compare the difficulty."""
		if isinstance(obj, Beatmap):
			return self.stars <= obj.stars

		raise TypeError("You can't compare an instance of Beatmap with another object")

	def _default_calculate(self):
		result = Beatmap.NO_MOD_MAX_CALCULATOR.calculate(self)
		self._stars, self._pp = result.stars, result.pp

	@property
	def version_fmt(self) -> int:
		return self.version
	
	@property
	def stars(self) -> float:
		return self._stars

	@property
	def pp(self) -> float:
		"""Return the max pp number (no mod)."""
		return self._pp
	
	@property
	def time(self) -> float:
		"""Return the difference between the first hit object and the last hit object"""
		hitobjects = self.hit_objects
		if len(hitobjects) > 1:
			return hitobjects[-1].end_time - hitobjects[0].start_time
		return 0
	
	@property
	def hitobjects_data(self) -> pd.DataFrame:
		hit_objects = self.hit_objects
		hit_objects_data = [
			{
				'Type': hit_obj.kind_str,
				'StartTime': hit_obj.start_time,
				'EndTime': hit_obj.end_time,
				'Sound': hit_obj.sound,
				'X': hit_obj.pos.x,
				'Y': hit_obj.pos.y,
				'PathType': np.nan if hit_obj.kind.path_type is None else hit_obj.kind.path_type,
				'Kind': str(hit_obj.kind.as_dict)
			}
			for hit_obj in hit_objects
		]
		return pd.DataFrame(hit_objects_data)

	def calculate(self, *args, **kwargs) -> osu_prelude.CalcResult:
		"""Create a `peace_performance_python.prelude.CalcResult` for this beatmap.
		
		Parameters
		----------
		arguments of `peace_performance_python.prelude.Calculator.__init__`
		"""
		return osu_prelude.Calculator(*args, **kwargs).calculate(self)

	def to_dataframe(self) -> pd.DataFrame:
		"""Return a DataFrame with metadatas of a beatmap."""
		data = {
			'Version_fmt': self.version_fmt,
			'Mode': self.mode,
			'Stars': self.stars,
			'PP': self.pp,
			'HP': self.hp,
			'CS': self.cs,
			'OD': self.od,
			'AR': self.ar,
			'SV': self.sv,
			'TickRate': self.tick_rate,
			'CountNormal': self.n_circles,
			'CountSlider': self.n_sliders,
			'CountSpinner': self.n_spinners,
			'Time': self.time
		}
		return pd.DataFrame(data=data, index=range(1), columns=data.keys())

	@classmethod
	def from_file(cls, filepath: str):
		"""Return a Beatmap instance with all data find in filepath."""
		beatmap = cls(filepath)
		return beatmap
