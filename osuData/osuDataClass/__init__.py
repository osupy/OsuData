"""
Project: OsuData

License MIT
Copyright © 2020-2021 - LostPy
"""

from .beatmap import Beatmap
from .beatmapSet import BeatmapSet
