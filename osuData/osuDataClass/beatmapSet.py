"""
Project: OsuData

License MIT
Copyright © 2020-2021 - LostPy
"""

import os
from datetime import datetime

import pandas as pd
from scipy.io import wavfile

import pydub
from pydub.playback import play

from .beatmap import Beatmap


class BeatmapSet:
	def __init__(self, folderpath: str, id_: int = None):
		self.folderpath = folderpath
		self.id = id_
		self.music_path = None
		self.title = None
		self.artist = None
		self.beatmaps = []
		self.date_add = None

	def __repr__(self) -> str:
		"""Return the representation of the dataframe of metadata."""
		return self.to_dataframe().__repr__()

	def __str__(self) -> str:
		"""Return the str of the dataframe of metadata."""
		return self.__repr__()

	def __len__(self) -> int:
		"""Number of beatmaps."""
		return len(self.beatmaps)

	def __eq__(self, obj) -> bool:
		"""Compare with the number of beatmaps."""
		if isinstance(obj, BeatmapSet):
			return len(self.beatmaps) == len(obj.beatmaps)

		raise TypeError("You can't compare an instance of BeatmapSet with another object")

	def __ne__(self, obj) -> bool:
		"""Compare with the number of beatmaps."""
		if isinstance(obj, BeatmapSet):
			return len(self.beatmaps) != len(obj.beatmaps)

		raise TypeError("You can't compare an instance of BeatmapSet with another object")

	def __gt__(self, obj) -> bool:
		"""Compare with the number of beatmaps."""
		if isinstance(obj, BeatmapSet):
			return len(self.beatmaps) > len(obj.beatmaps)

		raise TypeError("You can't compare an instance of BeatmapSet with another object")

	def __ge__(self, obj) -> bool:
		"""Compare with the number of beatmaps."""
		if isinstance(obj, BeatmapSet):
			return len(self.beatmaps) >= len(obj.beatmaps)

		raise TypeError("You can't compare an instance of BeatmapSet with another object")

	def __lt__(self, obj) -> bool:
		"""Compare with the number of beatmaps."""
		if isinstance(obj, BeatmapSet):
			return len(self.beatmaps) < len(obj.beatmaps)

		raise TypeError("You can't compare an instance of BeatmapSet with another object")

	def __le__(self, obj) -> bool:
		"""Compare with the number of beatmaps."""
		if isinstance(obj, BeatmapSet):
			return len(self.beatmaps) <= len(obj.beatmaps)

		raise TypeError("You can't compare an instance of BeatmapSet with another object")

	def __getitem__(self, index) -> Beatmap:
		"""Get beatmap with a index."""
		return self.beatmaps[index]

	def __setitem__(self, index, beatmap):
		"""Insert a beatmap in the list of beatmaps."""
		if isinstance(beatmap, Beatmap):
			self.beatmaps.insert(index, beatmap)

		raise TypeError("The value must be an instance of Beatmap")

	def __delitem__(self, index):
		"""Delete the beatmap with the index."""
		del(self.beatmaps[index])

	def __contains__(self, obj) -> bool:
		"""Return True if obj in the list of beatmaps."""
		return obj in self.beatmaps

	def append(self, beatmap):
		"""Append beatmap in the list of beatmaps."""
		if isinstance(beatmap, Beatmap):
			self.beatmaps.append(beatmap)

		raise TypeError("The value must be an instance of Beatmap")

	def pop(self, index=-1) -> Beatmap:
		"""Use pop method on the list of beatmaps."""
		return self.beatmaps.pop(index=index)

	def metadata(self) -> dict:
		"""Export BeatmapSet object in a dictionary."""
		return self.__dict__

	def load(self, mode: int = None):
		"""
		Initialize BeatmapSet object from files of beatmaps.
		Use `modes` argument if you want get specifics modes.
		"""
		self.date_add = datetime.fromtimestamp(os.path.getctime(self.folderpath)).strftime('%Y-%m-%d %H:%M:%S')
		file_name = os.path.basename(self.folderpath)
		try:
			self.id = int(file_name.split(' ')[0])
		except ValueError as e:  # Beatmaps not published
			return
		folderpath_data = file_name.split(str(self.id))[1].split(' - ', 1)

		if len(folderpath_data) > 1:
			self.title = folderpath_data[1].strip()
		else:
			self.title = ''
		self.artist = folderpath_data[0].strip()

		for name in os.listdir(self.folderpath):
			path = os.path.join(self.folderpath, name)
			if os.path.isfile(path) and name.endswith((".osu", )):
				if self.music_path is None:  # Initialize BeatmapSet music path
					with open(path, mode='r', encoding='utf8') as f:
						lines = f.readlines()
					self.music_path = lines[3][lines[3].find(" ")+1:]

				beatmap = Beatmap(path)
				if mode is None or beatmap.mode == mode:
					self.beatmaps.append(beatmap)

	def to_dataframe(self) -> pd.DataFrame:
		"""Export BeatmapSet object in a DataFrame."""
		if len(self.beatmaps) > 0:
			df = pd.concat([beatmap.to_dataframe() for beatmap in self.beatmaps], axis=0).reset_index(drop=True)
			df['Title'] = self.title
			df['Artist'] = self.artist
			df['Date_Add'] = self.date_add
		else:
			df = pd.DataFrame(columns=['Version_fmt', 'Mode', 'Title',
				'Stars', 'PP', 'HP', 'CS', 'OD', 'AR', 'SV',
				'TickRate', 'CountNormal', 'CountSlider', 'CountSpinner', 'Time', 'Date_Add'])
		return df

	def dataframe_hitobjects(self) -> pd.DataFrame:
		"""Return a DataFrame with hitobjects of all beatmaps from BeatmapSet object."""
		if len(self.beatmaps) > 0:
			return pd.concat([beatmap.hitobjects_data for beatmap in self.beatmaps], axis=0).reset_index(drop=True)
		return pd.DataFrame(columns=['Type', 'StartTime', 'EndTime', 'Sound', 'X', 'Y', 'PathType', 'Kind'])

	def to_csv(self, path: str = None):
		"""Export BeatmapSet object in a csv file."""
		return self.to_dataframe().to_csv(path, sep='$', index=False)

	def to_excel(self, path: str = None, sheet_name='', **kwargs):
		"""Export BeatmapSet object in a xlsx file."""
		sheet_name = self.title if sheet_name == '' else sheet_name
		return self.to_dataframe().to_excel(path, sheet_name=sheet_name, index=False, **kwargs)

	def mp3_object(self) -> pydub.AudioSegment:
		"""Return a mp3 object (module pydub)."""
		return pydub.AudioSegment.from_mp3(os.path.join(self.folderpath, self.music_path))

	def to_wav(self, name='audio_wav') -> str:
		"""Export the mp3 file in a wav file."""
		path = os.path.join(self.folderpath, name+'.wav')
		self.mp3_object().export(path, format="wav")
		return path

	def data_music(self):
		"""Extract audio data of the mp3 file."""
		path = self.to_wav()
		rate, audData = wavfile.read(path)
		os.remove(path)
		return rate, audData

	def music_to_dataframe(self) -> pd.DataFrame:
		"""Return a DataFrame with audio data of the music."""
		rate, audData = self.data_music()
		return pd.DataFrame(data=audData, columns=['L', 'R'])

	def play_music(self):
		"""Play the music. Note: This isn't a async method."""
		play(self.mp3_object())

	@classmethod
	def from_folder(cls, folderpath: str, mode: int = None):
		"""
		Return a BeatmapSet instance with all data find in folderpath.
		If `mode` is None, all beatmap are imported.
		"""
		beatmap_set = cls(folderpath)
		beatmap_set.load(mode=mode)
		return beatmap_set
